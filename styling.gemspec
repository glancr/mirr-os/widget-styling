# frozen_string_literal: true

require 'json'
$LOAD_PATH.push File.expand_path('lib', __dir__)

require 'styling/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = 'styling'
  s.version     = Styling::VERSION
  s.authors     = ['Tobias Grasse']
  s.email       = ['tg@glancr.de']
  s.homepage    = 'https://glancr.de/modules/styling'
  s.summary     = 'mirr.OS widget that provides visual styling.'
  s.description = 'Add visual dividers to your glancr screen.'
  s.license     = 'MIT'
  s.metadata    = { 'json' =>
                  {
                    type: 'widgets',
                    title: {
                      enGb: 'Styling',
                      deDe: 'Styling',
                      frFr: 'Styling',
                      esEs: 'Estilismo',
                      plPl: 'Stylizacja',
                      koKr: '스타일링'
                    },
                    description: {
                      enGb: s.description,
                      deDe: 'Füge deinem glancr visuelle Trenner hinzu.',
                      frFr: 'Ajoutez des séparateurs visuels à votre glancr.',
                      esEs: 'Añade separadores visuales a tu mirada.',
                      plPl: 'Dodaj wizualne separatory do glancr.',
                      koKr: 'glancr에 시각적 분배기를 추가하십시오.'
                    },
                    sizes: [
                      { w: 1, h: 1 },
                      { w: 2, h: 1 },
                      { w: 3, h: 1 },
                      { w: 4, h: 1 },
                      { w: 5, h: 1 },
                      { w: 6, h: 1 },
                      { w: 7, h: 1 },
                      { w: 8, h: 1 },
                      { w: 9, h: 1 },
                      { w: 10, h: 1 },
                      { w: 11, h: 1 },
                      { w: 12, h: 1 }
                    ],
                    languages: %i[enGb deDe frFr esEs plPl koKr],
                    group: nil
                  }.to_json
                }

  s.files = Dir['{app,config,db,lib}/**/*', 'MIT-LICENSE', 'Rakefile', 'README.md']

  s.add_development_dependency 'rails'
end
